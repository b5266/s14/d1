// JavaScript Comments
// This is a single line comment {ctrl/cmd + /}




// Syntax and Statements
/*
	- Statement in programming are instructions that we tell the computer to perform. It usually ends with semicolon (;)
	- A Syntax in programming is the set of rules that describes how statements must be constructed.
*/

console.log("Hello, world")

// Variable
// It is used to contain data
// Declaring a variable
/*Syntax
	let/const variableName;
*/

/*
		Guides in writing variables:
				1. Use the 'let'
*/
let myVariable;

console.log(myVariable);

// console.log(hello);

// let hello;

// Iniatializing Variables
/*Syntax
	let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.599;
const pi = 3.1416;

//
/*Syntax
	variableName
*/
productName = 'Laptop';
console.log	(productName);

// interest = 4.489;

//
let	supplier;

supplier = "John Smith trading";
console.log	(supplier);

// Multiple Variables
let	productCode = 'DC017', productBrand = 'Dell'
console.log(productCode, productBrand);

// Using a variable with a reserve keyword
/*const let = 'Hello';

console.log	(let);*/

// Data Types

/*Strings
	- series of characters that create a word, phrase, sentence or anything realtive to create text
*/
let	country ='Philippines';
let	province = "Metro Manila";

//Concentrating strings
// Multiple string values can be combined

let fullAddress = province + ', ' + country; // Metro Manila, Philippines - include every detail to execute and log value, example 'spacebar'
console.log(fullAddress);

let	greeting = 'I live in the' + country;
console.log(greeting);

// The escape character (\) in string
// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log	(message);
message	= 'John\'s employees went home early'
console.log	(message);

// Numbers
// Integers/whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers
let	grade = 98.7;
console.log	(grade)

// Exponential Notation
let planetDistance = 2e10;
console.log	(planetDistance);

// Combining text and numbers
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally  used to store values relating to the state of certain things
let isMarried = false;
let	inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays







let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//
let details = ["john", "Smith", 32, true];
console.log(details)

// Objects
// Objects







let	person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 092 8128", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

// Null
// It is used intentinally  to express the adsence of a value in a variable.
let spouse = null;
let myString = '';
let myNumber = 0;